/*
 * Bouncer notice for PostgreSQL
 *
 * 2023, Pierre Ducroquet
 */
#include "postgres.h"
#include "fmgr.h"

#include "commands/explain.h"
#include "optimizer/planner.h"
#include "catalog/pg_type.h"
#include "utils/guc.h"
#include "utils/guc_tables.h"

PG_MODULE_MAGIC;

void _PG_init(void);
void _PG_fini(void);

void
_PG_init(void)
{
#if PG_VERSION_NUM >= 160000
	/* Setup guc, looking into the hashtable through find_option */
	struct config_generic *guc = find_option("search_path", false, true, 0);
	if (guc)
		guc->flags |= GUC_REPORT;
	else
		elog(WARNING, "GUC not found for search_path");
#else
	/* find the guc for search_path by iterating through the table, and alter it */
	int num_guc;
	struct config_generic **gucs;

	num_guc = GetNumConfigOptions();
	gucs = get_guc_variables();
	for (int i = 0 ; i < num_guc ; i++)
	{
		if (strcmp(gucs[i]->name, "search_path") == 0)
		{
			gucs[i]->flags |= GUC_REPORT;
			break;
		}
	}
#endif
}

void
_PG_fini(void)
{
}

